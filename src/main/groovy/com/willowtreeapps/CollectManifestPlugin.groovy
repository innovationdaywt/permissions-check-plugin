package com.willowtreeapps

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project

class CollectManifestPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def evalPermissions = project.task("evaluatePermissions") {
            def sourceSet = project.android.sourceSets.getByName("main")
            def mainPermissions = ManifestPermissionsReader.newInstance().getPermissions(sourceSet.manifest.srcFile)
            project.android.applicationVariants.all { variant ->
                variant.outputs.each { output ->
                    output.processManifest.doLast {
                        String manifestPath = ""
                        try {
                            manifestPath = "$manifestOutputDirectory/AndroidManifest.xml"
                        } catch (MissingPropertyException mpe) {
                            println "MESSAGE: Android Plugin is not 3.0+. Now using 2.0 properties"
                            manifestPath = manifestOutputFile
                        }

                        if (manifestPath.isEmpty()) {
                            println("manifest output directory not found")
                        }

                        def manifestOutputFileFromPath = new File(manifestPath)
                        def variantPermissions = ManifestPermissionsReader.newInstance().getPermissions(manifestOutputFileFromPath)
                        def extraPermission = variantPermissions - mainPermissions
                        if (extraPermission.size() > 0) {
                            println("ERROR:  ${variant.name.capitalize()} has ${extraPermission.size()} extra permissions, listed below")
                            println(extraPermission.join("\n"))
                            throw new GradleException("Variant: ${variant.name.capitalize()} contains permissions not declared in main manifest. See output for details.")
                        }
                    }
                }
            }
        }

        evalPermissions.group = "Verification"
        evalPermissions.description = "Runs after evaluation, compares permissions in merged manifests with main manifest"

        project.afterEvaluate {
            evalPermissions
        }


    }
}
