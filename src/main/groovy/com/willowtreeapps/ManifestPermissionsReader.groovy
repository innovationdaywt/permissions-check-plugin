package com.willowtreeapps

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler

import javax.xml.parsers.SAXParserFactory

Set<String> getPermissions(File manifest) {
    def parser = SAXParserFactory.newInstance().newSAXParser()
    Set<String> permissions = new HashSet<>()

    def handler = new DefaultHandler() {
        @Override
        void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes)
            if (qName.equalsIgnoreCase("uses-permission")) {
                permissions.add(attributes.getValue("android:name"))
            }
        }
    }
    parser.parse(manifest, handler)
    return permissions
}